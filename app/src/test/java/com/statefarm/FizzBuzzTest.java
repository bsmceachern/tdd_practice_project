package com.statefarm;

/*
Create a single method or function that accepts an integer, and returns a string, with the following acceptance criteria.

If the number is divisable by 3, return the string "Fizz"
If the number is divisable by 5, return the string "Buzz"
If the number is divisable by both 3 and 5, return the string "FizzBuzz"
Otherwise, return the number as a string

 */
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {

    @Test
    public void returnStringFizz() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Fizz", fizzBuzz.fizzBuzzEvaluator(9));
    }

    @Test
    public void returnStringFizzIfDivisibleByThree() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Fizz", fizzBuzz.fizzBuzzEvaluator(12));
    }

    @Test
    public void shouldReturnBuzzWithTen() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Buzz", fizzBuzz.fizzBuzzEvaluator(10));
    }

    @Test
    public void shouldReturnFizzBuzzWith15() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("FizzBuzz", fizzBuzz.fizzBuzzEvaluator(15));
    }

    @Test
    public void shouldReturnInputNumberAsString() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("11", fizzBuzz.fizzBuzzEvaluator(11));
    }
}
