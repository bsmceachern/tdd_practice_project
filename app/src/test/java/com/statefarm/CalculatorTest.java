package com.statefarm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    @Test
    public void return0WithEmptyString() {
        Calculator calculator = new Calculator();
        assertEquals(0, calculator.add(""));
    }

    @Test
    public void return1NumberAsString() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add("2"));
    }

    @Test
    public void combine2Numbers() {
        Calculator calculator = new Calculator();
        assertEquals(4, calculator.add("2,2"));
    }

    @Test
    public void combineALotOfNumbers() {
        Calculator calculator = new Calculator();
        assertEquals(100, calculator.add("10,20,30,40"));

    }

    @Test
    public void noNegativeNumbersAllowed() {
        Calculator calculator = new Calculator();
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            calculator.add("-1, 1");
        });
        assertEquals("negatives not allowed - -1",exception.getMessage());
    }

    @Test
    public void allowVariousDelimiters() {
        Calculator calculator = new Calculator();
        assertEquals(10, calculator.add("3\n5,2"));
    }

    @Test
    public void allowMoreDelimiters() {
        Calculator calculator = new Calculator();
        assertEquals(3, calculator.add("//;\n1;2"));
    }
}
