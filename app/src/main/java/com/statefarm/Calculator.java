package com.statefarm;

import java.util.Objects;

public class Calculator {

    /*create a simple string calculator with a method signature(method name needs to be called *int add*:
       int add(string numbers);
       numbers will come in as strings
        method can handle up to 2 string numbers, separated by a comma
       start by handling edge case of an empty string
    */


    public int add(String numbers) {
        int result = 0;
        if (numbers == "") {
            return result;
        } else {
            // create a new string to numbers.replace() all \n with ,space
            String betterNumbers = numbers.replace('\n', ',');
            String bestNumbers = betterNumbers.replace(';', ',');
            String[] numArray = bestNumbers.split(",");
            for (String number:  numArray) {
                if(Objects.equals(number, "") || Objects.equals(number, "//")) {
                    continue;
                }
                if (Integer.valueOf(number) < 0) {
                    throw new IllegalArgumentException("negatives not allowed - " + number);
                }
                result += Integer.valueOf(number);
            }
        }
        return result;
            }

}
